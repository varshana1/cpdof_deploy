artifactory_url="http://localhost:8081/artifactory"

repo="libs-snapshot-local"

artifacts="com/agiletestingalliance/cpdofwebappsep/CPDOFWebAppSep19"          

url=$artifactory_url/$repo/$artifacts

file=`curl -s $url/maven-metadata.xml`

version=`curl -s $url/maven-metadata.xml | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`

build=`curl -s $url/$version/maven-metadata.xml | grep '<value>' |head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"`

BUILD_LATEST="$url/$version/CPDOFWebAppSep19-1.2-September-20190909.122530-2.war"
echo "File Name  = " +$BUILD_LATEST

echo $BUILD_LATEST > filename.txt
